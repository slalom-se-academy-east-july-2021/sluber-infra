# Sluber Infrastructure

This project uses CloudFormation templates to:

* create new environment with VPC, subnets, ECS, RDS and security groups
* deploy fargate tasks, fargate services, ALB


## Deployment instructions

Create new environment:

```
aws cloudformation create-stack --region us-west-2 --stack-name sluber --capabilities CAPABILITY_IAM --template-body file://infra.yml
```

Update enviroment (manual step below, normally done with pipeline)


```
aws cloudformation update-stack \
--stack-name sluber \
--capabilities CAPABILITY_IAM \
--template-body file://infra.yml \
--parameters  \
ParameterKey=foo,ParameterValue=bar
```
